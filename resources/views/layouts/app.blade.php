<!DOCTYPE html>

<html lang="en">


<head>

	<!-- META -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="" />
    <meta name="author" content="" />
    <meta name="robots" content="" />    
    <meta name="description" content="Rupert Service Ltd." />
    
    <!-- FAVICONS ICON -->
    <link rel="icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.png" />
    
    <!-- PAGE TITLE HERE -->
    <title>Rupert Service Ltd.</title>
    
    <!-- MOBILE SPECIFIC -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- [if lt IE 9]>
        <script src="js/html5shiv.min.js"></script>
        <script src="js/respond.min.js"></script>
	<![endif] -->
    
    <!-- BOOTSTRAP STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/bootstrap.min.css') }}">
    <!-- FONTAWESOME STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/fontawesome/css/font-awesome.min.css') }}" />
    <!-- FLATICON STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/flaticon.min.css') }}">
    <!-- ANIMATE STYLE SHEET --> 
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/animate.min.css') }}">
    <!-- OWL CAROUSEL STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/owl.carousel.min.css') }}">
    <!-- BOOTSTRAP SELECT BOX STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/bootstrap-select.min.css') }}">
    <!-- MAGNIFIC POPUP STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/magnific-popup.min.css') }}">
    <!-- LOADER STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/loader.min.css') }}">    
    <!-- MAIN STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/style.css') }}">
    <!-- THEME COLOR CHANGE STYLE SHEET -->
    <link rel="stylesheet" class="skin" type="text/css" href="{{ asset ('css/skin/skin-1.css') }}">
    <!-- CUSTOM  STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/custom.css') }}">
    <!-- SIDE SWITCHER STYLE SHEET -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('css/switcher.css') }}">    

    
    <!-- REVOLUTION SLIDER CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('plugins/revolution/revolution/css/settings.css') }}">
    <!-- REVOLUTION NAVIGATION STYLE -->
    <link rel="stylesheet" type="text/css" href="{{ asset ('plugins/revolution/revolution/css/navigation.css') }}">
    
    <!-- GOOGLE FONTS -->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,300italic,400italic,500,500italic,700,700italic,900italic,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,800italic,800,700italic' rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=Crete+Round:400,400i&amp;subset=latin-ext" rel="stylesheet">

    <style type="text/css">
        
        html {
  scroll-behavior: smooth;
}
    </style>
    
</head>

<body id="bg">

	<div class="page-wraper"> 
       	
        <!-- HEADER START -->
        <header class="site-header header-style-1 ">
        
            <div class="top-bar bg-secondry">
                <div class="container">
                    <div class="row">
                        <div class="wt-topbar-right clearfix">
                        	<ul class="social-bx list-inline pull-right">
                                <li><a href="javascript:void(0);" class="fa fa-facebook"></a></li>
                                <li><a href="javascript:void(0);" class="fa fa-twitter"></a></li>
                                <li><a href="javascript:void(0);" class="fa fa-linkedin"></a></li>
                             
                            </ul>
                            <ul class="list-unstyled e-p-bx pull-right">
                                <li><i class="fa fa-envelope"></i>info@rupertservices.co.uk</li>
                               <!--  <li><i class="fa fa-phone"></i>Phone Number</li> -->
                            </ul>
                            
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="sticky-header main-bar-wraper">
                <div class="main-bar bg-white">
                    <div class="container">
                        <div class="logo-header">
                            <a href="{{ url ('/home#home') }}">
                                <img src="{{ asset ('images/logo.png') }}" width="171" height="49" alt="" />
                            </a>
                        </div>
                        <!-- NAV Toggle Button -->
                        <button data-target=".header-nav" data-toggle="collapse" type="button" class="navbar-toggle collapsed">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                      
           
                        <!-- MAIN Vav -->
                        <div class="header-nav navbar-collapse collapse ">
                            <ul class=" nav navbar-nav">
                                <li class="active">
                                    <a href="{{ url ('/home#home') }}">Home</a> 
                                </li>

                                <li class="active">
                                    <a href="{{ url ('/home#about') }}">About Us</a> 
                                </li>

                                <li class="afctive">
                                    <a href="{{ url ('/home#services') }}">Services</a> 
                                </li>

                                <li class="active">
                                    <a href="{{ url ('/home#contact') }}">Contact Us</a> 
                                </li>
                        
                          
                              
                                
                       
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            
        </header>
        <!-- HEADER END -->


        @yield('content')


                <!-- FOOTER START -->
        <footer class="site-footer footer-light">
            <!-- COLL-TO ACTION START -->
            <div class="call-to-action-wrap bg-primary bg-repeat" style="background-image:url(images/background/bg-4.png);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8 col-sm-8">
                            <div class="call-to-action-left p-tb20 p-r50">
                                <h4 class="text-uppercase m-b10">We are ready to build your dream tell us more about your project</h4>
                        
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="call-to-action-right p-tb30">
                                <a href="{{ url ('/contact-us') }}" class="site-button-secondry text-uppercase font-weight-600">
                                    Contact us  <i class="fa fa-angle-double-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
       
            <!-- FOOTER COPYRIGHT -->
            <div class="footer-bottom overlay-wraper">
                <div class="overlay-main"></div>
                <div class="constrot-strip"></div>
                <div class="container p-t30">
                    <div class="row">
                        <div class="wt-footer-bot-left">
                            <span class="copyrights-text">© 2020 Rupert Service. All Rights Reserved.</span>
                        </div>
                    
                    </div>
                </div>
            </div>
        </footer>
        <!-- FOOTER END -->

        
        <!-- SCROLL TOP BUTTON -->
        <button class="scroltop"><span class=" iconmoon-house relative" id="btn-vibrate"></span>Top</button>
        
    </div>
 

<!-- JAVASCRIPT  FILES ========================================= --> 
<script type="text/javascript"  src="{{ asset ('js/jquery-1.12.4.min.js') }}"></script><!-- JQUERY.MIN JS -->
<script type="text/javascript"  src="{{ asset ('js/bootstrap.min.js') }}"></script><!-- BOOTSTRAP.MIN JS -->

<script type="text/javascript"  src="{{ asset ('js/bootstrap-select.min.js') }}"></script><!-- FORM JS -->
<script type="text/javascript"  src="{{ asset ('js/jquery.bootstrap-touchspin.min.js') }}"></script><!-- FORM JS -->

<script type="text/javascript"  src="{{ asset ('js/magnific-popup.min.js') }}"></script><!-- MAGNIFIC-POPUP JS -->

<script type="text/javascript"  src="{{ asset ('js/waypoints.min.js') }}"></script><!-- WAYPOINTS JS -->
<script type="text/javascript"  src="{{ asset ('js/counterup.min.js') }}"></script><!-- COUNTERUP JS -->
<script type="text/javascript"  src="{{ asset ('js/waypoints-sticky.min.js') }}"></script><!-- COUNTERUP JS -->

<script type="text/javascript" src="{{ asset ('js/isotope.pkgd.min.js') }}"></script><!-- MASONRY  -->

<script type="text/javascript"  src="{{ asset ('js/owl.carousel.min.js') }}"></script><!-- OWL  SLIDER  -->

<script type="text/javascript"  src="{{ asset ('js/stellar.min.js') }}"></script><!-- PARALLAX BG IMAGE   --> 
<script type="text/javascript"  src="{{ asset ('js/scrolla.min.js') }}"></script><!-- ON SCROLL CONTENT ANIMTE   -->

<script type="text/javascript"  src="{{ asset ('js/custom.js') }}"></script><!-- CUSTOM FUCTIONS  -->
<script type="text/javascript"  src="{{ asset ('js/shortcode.js') }}"></script><!-- SHORTCODE FUCTIONS  -->
<script type="text/javascript"  src="{{ asset ('js/switcher.js') }}"></script><!-- SWITCHER FUCTIONS  -->


<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="{{ asset ('plugins/revolution/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset ('plugins/revolution/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->    
<script type="text/javascript" src="{{ asset ('plugins/revolution/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script type="text/javascript" src="{{ asset ('plugins/revolution/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset ('plugins/revolution/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script type="text/javascript" src="{{ asset ('plugins/revolution/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset ('plugins/revolution/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script type="text/javascript" src="{{ asset ('plugins/revolution/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset ('plugins/revolution/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset ('plugins/revolution/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script type="text/javascript" src="{{ asset ('plugins/revolution/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>

<!-- REVOLUTION SLIDER SCRIPT FILES -->
<script type="text/javascript" src="{{ asset ('js/rev-script-2.js') }}"></script>






</body>


</html>