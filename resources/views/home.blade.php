@extends('layouts.app')


@section('content')


      <section id="home">
        
            <!-- SLIDER START -->
            <div class="main-slider style-two default-banner">
           		<div class="tp-banner-container">
                    <div class="tp-banner" >
                        <!-- START REVOLUTION SLIDER 5.4.1 -->
                        <div id="rev_slider_1014_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="typewriter-effect" data-source="gallery">
                           <div id="rev_slider_1014_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.1">
                                <ul>
                                    <!-- SLIDE 1 -->
                                    <li data-index="rs-2000" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="{{ asset ('images/main-slider/slider2/slide2.jpg') }}"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                        <!-- MAIN IMAGE -->
                                        <img src="{{ asset ('images/main-slider/slider2/slide2.jpg') }}"  alt=""  data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina/>
                                        <!-- LAYERS -->
                                        
                                        <!-- LAYER NR. 1 [ for overlay ] -->
                                        <div class="tp-caption tp-shape tp-shapewrapper " 
                                        id="slide-200-layer-1" 
                                        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                                        data-width="full"
                                        data-height="full"
                                        data-whitespace="nowrap"
                                        data-type="shape" 
                                        data-basealign="slide" 
                                        data-responsive_offset="off" 
                                        data-responsive="off"
                                        data-frames='[
                                        {"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},
                                        {"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}
                                        ]'
                                        data-textAlign="['left','left','left','left']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        
                                        style="z-index: 12;background-color:rgba(0, 0, 0, 0.10);border-color:rgba(0, 0, 0, 0);border-width:0px;"> 
                                        </div>
                                    
                                        <!-- LAYER NR. 2 [ for title ] -->
                                        <div class="tp-caption   tp-resizeme" 
                                        id="slide-200-layer-2" 
                                        data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']" 
                                        data-y="['top','top','top','top']" data-voffset="['300','300','300','300']"  
                                        data-fontsize="['60','60','60','50']"
                                        data-lineheight="['70','70','70','60']"
                                        data-width="['700','700','700','700']"
                                        data-height="['none','none','none','none']"
                                        data-whitespace="['normal','normal','normal','normal']"
                                    
                                        data-type="text" 
                                        data-responsive_offset="on" 
                                        data-frames='[
                                        {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                        {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                        ]'
                                        data-textAlign="['left','left','left','left']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                    
                                        style="z-index: 13; 
                                        white-space: normal; 
                                        font-weight: 700; 
                                        color: #ffbc13;
                                        border-width:0px;">
                                        <span class="text-uppercase" style="font-family:'Roboto' ;">Welcome To Rupert Services Ltd.</span>
                                        </div>
                                    
                                        <!-- LAYER NR. 3 [ for paragraph] -->
                                        <div class="tp-caption  tp-resizeme" 
                                        id="slide-200-layer-3" 
                                        data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']" 
                                        data-y="['top','top','top','top']" data-voffset="['400','400','400','400']"  
                                        data-fontsize="['18','18','18','30']"
                                        data-lineheight="['30','30','30','40']"
                                        data-width="['650','650','650','650']"
                                        data-height="['none','none','none','none']"
                                        data-whitespace="['normal','normal','normal','normal']"
                                    
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[
                                        {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":1000,"ease":"Power4.easeOut"},
                                        {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                        ]'
                                        data-textAlign="['left','left','left','left']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                    
                                        style="z-index: 13; 
                                        font-weight: 500; 
                                        color: rgba(255, 255, 255, 0.85);
                                        border-width:0px;">
                                       
                                        </div>
                                    
                                
                                        
                                        <!-- LAYER NR. 5 [ for worker pic 1 big] -->
                                        <div class="tp-caption tp-resizeme" 	
                                        id="slide-200-layer-5"						
                                        data-x="['right','right','right','right']" data-hoffset="['50','50','50','50']" 
                                        data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','0','0']"
                                        
                                        data-frames='[ 
                                        {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":3000,"ease":"Power4.easeOut"},
                                        {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                        ]'
                                        
                                        style="z-index: 13;">
                                        <img src="{{ asset ('images/main-slider/slider2/banner2-1.png') }}" width="400" alt="">
                                        </div>
                                              
                                        <!-- LAYER NR. 6 [ for worker pic 2 small ] -->
                                        <div class="tp-caption tp-resizeme" 	
                                        id="slide-200-layer-6"						
                                        data-x="['right','right','right','right']" data-hoffset="['-250','-250','-250','-250']" 
                                        data-y="['top','top','top','top']" data-voffset="['320','280','280','280']"
                                        
                                        data-frames='[ 
                                        {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":4000,"ease":"Power4.easeOut"},
                                        {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                        ]'
                                        
                                        style="z-index: 13;">
                                        <img src="{{ asset ('images/main-slider/slider2/banner2-2.png') }}" width="400" alt="">
                                        </div> 
                                        
                                        <!-- LAYER NR. 7 [ for worker pic road white line] -->
                                        <div class="tp-caption tp-resizeme" 	
                                        id="slide-200-layer-7"						
                                        data-x="['right','right','right','right']" data-hoffset="['-150','-150','-150','-150']" 
                                        data-y="['bottom','bottom','bottom','bottom']" data-voffset="['100','100','100','100']"
                                        
                                        data-frames='[ 
                                        {"from":"y:0px(R);opacity:0;","speed":3000,"to":"o:1;","delay":5000,"ease":"Power4.easeOut"},
                                        {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                        ]'
                                        
                                        style="z-index: 13;">
                                        <img src="{{ asset ('images/main-slider/slider2/banner2-3.png') }}" width="400" alt="">
                                        </div>                                
                                        
                                                                      
                                    </li>
                                    
                                    <!-- SLIDE 2 -->
                                    <li data-index="rs-3000" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="{{ asset ('images/main-slider/slider2/slide3.jpg') }}"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                    
                                        <!-- MAIN IMAGE -->
                                        <img src="{{ asset ('images/main-slider/slider2/slide3.jpg') }}"  alt=""  data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina/>
                                        <!-- LAYERS -->
                                        
                                        <!-- LAYER NR. 1 [ for overlay ] -->
                                        <div class="tp-caption tp-shape tp-shapewrapper " 
                                        id="slide-300-layer-1" 
                                        data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                        data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                                        data-width="full"
                                        data-height="full"
                                        data-whitespace="nowrap"
                                        data-type="shape" 
                                        data-basealign="slide" 
                                        data-responsive_offset="off" 
                                        data-responsive="off"
                                        data-frames='[
                                        {"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},
                                        {"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}
                                        ]'
                                        data-textAlign="['left','left','left','left']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                        
                                        style="z-index: 12;background-color:rgba(0, 0, 0, 0.10);border-color:rgba(0, 0, 0, 0);border-width:0px;"> 
                                        </div>
                                    
                                        <!-- LAYER NR. 2 [ for title ] -->
                                        <div class="tp-caption   tp-resizeme" 
                                        id="slide-300-layer-2" 
                                        data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']" 
                                        data-y="['top','top','top','top']" data-voffset="['300','300','300','300']"  
                                        data-fontsize="['60','60','60','50']"
                                        data-lineheight="['70','70','70','60']"
                                        data-width="['700','700','700','700']"
                                        data-height="['none','none','none','none']"
                                        data-whitespace="['normal','normal','normal','normal']"
                                    
                                        data-type="text" 
                                        data-responsive_offset="on" 
                                        data-frames='[
                                        {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                        {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                        ]'
                                        data-textAlign="['left','left','left','left']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                    
                                        style="z-index: 13; 
                                        white-space: normal; 
                                        font-weight: 700; 
                                        color: #ffbc13;
                                        border-width:0px;">
                                        <span class="text-uppercase" style="font-family:'Roboto' ;">We Build Your Dream</span>
                                        </div>
                                    
                                        <!-- LAYER NR. 3 [ for paragraph] -->
                                        <div class="tp-caption  tp-resizeme" 
                                        id="slide-300-layer-3" 
                                        data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']" 
                                        data-y="['top','top','top','top']" data-voffset="['400','400','400','400']"  
                                        data-fontsize="['18','18','18','30']"
                                        data-lineheight="['30','30','30','40']"
                                        data-width="['650','650','650','650']"
                                        data-height="['none','none','none','none']"
                                        data-whitespace="['normal','normal','normal','normal']"
                                    
                                        data-type="text" 
                                        data-responsive_offset="on"
                                        data-frames='[
                                        {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":1000,"ease":"Power4.easeOut"},
                                        {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                        ]'
                                        data-textAlign="['left','left','left','left']"
                                        data-paddingtop="[0,0,0,0]"
                                        data-paddingright="[0,0,0,0]"
                                        data-paddingbottom="[0,0,0,0]"
                                        data-paddingleft="[0,0,0,0]"
                                    
                                        style="z-index: 13; 
                                        font-weight: 500; 
                                        color: rgba(255, 255, 255, 0.85);
                                        border-width:0px;">
                                       
                                        </div>
                                    
                                        
                                        <!-- LAYER NR. 5 [ for worker pic ] -->
                                        <div class="tp-caption tp-resizeme" 	
                                        id="slide-300-layer-5"						
                                        data-x="['right','right','right','right']" data-hoffset="['-200','-100','-50','-10']" 
                                        data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','0','0']"
                                        
                                        data-frames='[ 
                                        {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeOut"},
                                        {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                        ]'
                                        
                                        style="z-index: 13;">
                                        <img src="{{ asset ('images/main-slider/slider2/banner3-1.png') }}" width="400" alt="">
                                        </div>    
                                    
                                    </li>
                                    
                                    <!-- SLIDE 3 -->	
                                    <li data-index="rs-1000" data-transition="slidingoverlayhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="{{ asset ('images/main-slider/slider2/slide1.jpg') }}"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                                    <!-- MAIN IMAGE -->
                                    <img src="{{ asset ('images/main-slider/slider2/slide1.jpg') }}"  alt=""  data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina/>
                                    <!-- LAYERS -->
                                    
                                    <!-- LAYER NR. 1 [ for overlay ] -->
                                    <div class="tp-caption tp-shape tp-shapewrapper " 
                                    id="slide-100-layer-1" 
                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" 
                                    data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" 
                                    data-width="full"
                                    data-height="full"
                                    data-whitespace="nowrap"
                                    data-type="shape" 
                                    data-basealign="slide" 
                                    data-responsive_offset="off" 
                                    data-responsive="off"
                                    data-frames='[
                                    {"from":"opacity:0;","speed":1000,"to":"o:1;","delay":0,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"opacity:0;","ease":"Power4.easeOut"}
                                    ]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"
                                    
                                    style="z-index: 12;background-color:rgba(0, 0, 0, 0.10);border-color:rgba(0, 0, 0, 0);border-width:0px;"> 
                                    </div>
                                
                                    <!-- LAYER NR. 2 [ for title ] -->
                                    <div class="tp-caption   tp-resizeme" 
                                    id="slide-100-layer-2" 
                                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']" 
                                    data-y="['top','top','top','top']" data-voffset="['300','300','300','300']"  
                                    data-fontsize="['60','60','60','50']"
                                    data-lineheight="['70','70','70','60']"
                                    data-width="['700','700','700','700']"
                                    data-height="['none','none','none','none']"
                                    data-whitespace="['normal','normal','normal','normal']"
                                
                                    data-type="text" 
                                    data-responsive_offset="on" 
                                    data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"
                                
                                    style="z-index: 13; 
                                    white-space: normal; 
                                    font-weight: 700; 
                                    color: #ffbc13;
                                    border-width:0px;">
                                    <span class="text-uppercase" style="font-family:'Roboto' ;">We Build Your Dream</span>
                                    </div>
                                
                                    <!-- LAYER NR. 3 [ for paragraph] -->
                                    <div class="tp-caption  tp-resizeme" 
                                    id="slide-100-layer-3" 
                                    data-x="['left','left','left','left']" data-hoffset="['30','30','30','100']" 
                                    data-y="['top','top','top','top']" data-voffset="['400','400','400','400']"  
                                    data-fontsize="['18','18','18','30']"
                                    data-lineheight="['30','30','30','40']"
                                    data-width="['650','650','650','650']"
                                    data-height="['none','none','none','none']"
                                    data-whitespace="['normal','normal','normal','normal']"
                                
                                    data-type="text" 
                                    data-responsive_offset="on"
                                    data-frames='[
                                    {"from":"y:100px(R);opacity:0;","speed":2000,"to":"o:1;","delay":1000,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                    data-textAlign="['left','left','left','left']"
                                    data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]"
                                    data-paddingbottom="[0,0,0,0]"
                                    data-paddingleft="[0,0,0,0]"
                                
                                    style="z-index: 13; 
                                    font-weight: 500; 
                                    color: rgba(255, 255, 255, 0.85);
                                    border-width:0px;">
                                   
                                    </div>
                                
                              
                                    
                                    <!-- LAYER NR. 5 [ for worker pic ] -->
                                    <div class="tp-caption tp-resizeme" 	
                                    id="slide-100-layer-5"						
                                    data-x="['right','right','right','right']" data-hoffset="['-200','-100','-50','-100']" 
                                    data-y="['bottom','bottom','bottom','bottom']" data-voffset="['0','0','0','0']"
                                    
                                    data-frames='[ 
                                    {"from":"y:200px(R);opacity:0;","speed":2000,"to":"o:1;","delay":2000,"ease":"Power4.easeOut"},
                                    {"delay":"wait","speed":1000,"to":"y:-50px;opacity:0;","ease":"Power2.easeInOut"}
                                    ]'
                                    
                                    style="z-index: 13;">
                                    <img src="{{ asset ('images/main-slider/slider2/banner1-1.png') }}" width="400" alt="">
                                    </div>                            
                                    
                                  </li>
                                </ul>
                                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	
                           </div>
                        </div>
                        <!-- END REVOLUTION SLIDER -->
                    </div>
            	</div>
            </div>
            <!-- SLIDER END -->

</section>
               <!-- OUR VALUE SECTION START -->           
             <div class="section-full bg-primary">
                <div class="container our-value">
                     <div class="row"> 
                        <div class="col-md-8 col-sm-8 p-t15 p-b30 our-value-left">
                             <div class="clearfix">
                                <div class="col-md-6 p-tb10">
                                    <div class="wt-icon-box-wraper left ">
                                        <div class="icon-md">
                                            <div  class="icon-cell text-white">
                                                <span class="iconmoon-buildings"></span>
                                            </div>
                                        </div>
                                        <div class="icon-content text-secondry">
                                            <h5 class="wt-tilte text-uppercase m-b5">best quality</h5>
                                       
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 p-tb10">
                                    <div class="wt-icon-box-wraper left">
                                        <div class="icon-md text-primary">
                                            <div  class="icon-cell text-white">
                                                <span class="iconmoon-hours"></span>
                                            </div>
                                        </div>
                                        <div class="icon-content text-secondry">
                                            <h5 class="wt-tilte text-uppercase m-b0">24 hour support</h5>
                                       
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 p-t50 p-b50 col-md-offset-1 our-value-right">
                            <div class="">
                                <a href="{{ url ('/contact-us') }}" class="site-button-secondry  m-r15 text-uppercase font-weight-600">Contact us</a>
                            </div>
                        </div>
                     </div>
                </div>
             </div>
            <!-- OUR VALUE SECTION  END -->

               <!-- WHY CHOOSE US SECTION START  -->
            <div class="section-full bg-gray p-t80 p-b120">
            	<div class="container">
                    <!-- TITLE START-->
                    <div class="section-head text-center">
                        <h2 class="text-uppercase">Why Choose us</h2>
                        <div class="wt-separator-outer">
                            <div class="wt-separator style-square">
                                <span class="separator-left bg-primary"></span>
                                <span class="separator-right bg-primary"></span>
                            </div>
                        </div>
                        <p>Every project we execute is unique and personalized as per the clients’ need. We fabricate projects that are extraordinary in their own way. By sighting and accomplishing genuine points of difference in conceptualization, we guarantee finer returns and quality to our clients.</p>
                    </div>
                    <!-- TITLE END-->
                    <div class="section-content no-col-gap">
                        <div class="row">
                        
                            <!-- COLUMNS 1 -->
                            <div class="col-md-4 col-sm-6 animate_line">
                                <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                                    <div class="icon-sm text-primary m-b20">
                                        <a href="#" class="icon-cell"><i class="fa fa-life-ring" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="icon-content">
                                        <h5 class="wt-tilte text-uppercase">BEST QUALITY</h5>
                                       
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 2 -->
                            <div class="col-md-4 col-sm-6 animate_line">
                                <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                                    <div class="icon-sm text-primary m-b20">
                                        <a href="#" class="icon-cell"><i class="fa fa-trophy" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="icon-content ">
                                        <h5 class="wt-tilte text-uppercase">INTEGRITY</h5>
                                       
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 3 -->
                            <div class="col-md-4 col-sm-6 animate_line">
                                <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                                    <div class="icon-sm text-primary m-b20">
                                        <a href="#" class="icon-cell"><i class="fa fa-thumbs-up" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="icon-content">
                                        <h5 class="wt-tilte text-uppercase">STRATEGY</h5>
                                       
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 4 -->
                            <div class="col-md-4 col-sm-6 animate_line">
                                <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                                    <div class="icon-sm text-primary m-b20">
                                        <a href="#" class="icon-cell"><i class="fa fa-users" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="icon-content">
                                        <h5 class="wt-tilte text-uppercase">SAFETY</h5>
                                       
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 5 -->
                            <div class="col-md-4 col-sm-6 animate_line">
                                <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                                    <div class="icon-sm text-primary m-b20">
                                        <a href="#" class="icon-cell"><i class="fa fa-area-chart" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="icon-content">
                                        <h5 class="wt-tilte text-uppercase">COMMUNITY</h5>
                                       
                                    </div>
                                </div>
                            </div>
                            <!-- COLUMNS 6 -->
                            <div class="col-md-4 col-sm-6 animate_line">
                                <div class="wt-icon-box-wraper  p-a30 center bg-white m-a5">
                                    <div class="icon-sm text-primary m-b20">
                                        <a href="#" class="icon-cell"><i class="fa fa-cogs" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="icon-content">
                                        <h5 class="wt-tilte text-uppercase">SUSTAINABILITY</h5>
                                       
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <!-- WHY CHOOSE US SECTION END  --> 


<section id="about">

              <!-- ABOUT COMPANY SECTION START -->
            <div class="section-full p-tb100 bg-gray">
                <div class="container">
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-6 col-xs-100pc">
                            <div class="about-com-pic">
                                <img src="images/about-pic.jpg" alt="" class="img-responsive"/>
                            </div>
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-6 col-xs-100pc">
                            <div class="section-head text-left">
                                <h2 class="text-uppercase">About Company </h2>
                                <div class="wt-separator-outer">
                                    <div class="wt-separator style-square">
                                        <span class="separator-left bg-primary"></span>
                                        <span class="separator-right bg-primary"></span>
                                    </div>
                                </div>
                                <p align="justify">We are eagerly waiting to help you build your dream home/buidings. We take pride in our technical expertise, diligent know-how and consummate discipline to devise every home that not only supersedes your hope but also carry out the said task on time and within your budget.
                                </p>
                                <p align="justify">
                               We guarantee that the property handing over process will be hassle and stress-free for our customers. We take pride that we have a broad range of services, from setting up the initial requisites to handing over the keys. After all, when you are on the verge of developing your own beloved home, you make sure not to miss out on any single point, as it is an investment of your hard earned money.
                                </p>
                            </div>

                        </div>
                        <div class="col-md-7 col-sm-12 col-xs-12">
                            <div class="about-types row">
                                <div class="col-md-6 col-sm-6 col-xs-6 col-xs-100pc p-tb20">
                                    <div class="wt-icon-box-wraper left">
                                        <div class="icon-sm text-primary">
                                            <a href="#" class="icon-cell p-t5 center-block"><i class="fa fa-building" aria-hidden="true"></i></a>
                                        </div>
                                        <div class="icon-content">
                                            <h5 class="wt-tilte text-uppercase m-b0">Building</h5>
                                         
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 col-xs-100pc p-tb20 ">
                                    <div class="wt-icon-box-wraper left">
                                        <div class="icon-sm text-primary">
                                            <a href="#" class="icon-cell p-t5 center-block"><i class="fa fa-paint-brush" aria-hidden="true"></i></a>
                                        </div>
                                        <div class="icon-content">
                                            <h5 class="wt-tilte text-uppercase m-b0">Renovation</h5>
                                         
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 col-xs-100pc p-tb20 ">
                                    <div class="wt-icon-box-wraper left">
                                        <div class="icon-sm text-primary">
                                            <a href="#" class="icon-cell p-t5 center-block"><i class="fa fa-gavel" aria-hidden="true"></i></a>
                                        </div>
                                        <div class="icon-content">
                                            <h5 class="wt-tilte text-uppercase m-b0 ">Digging</h5>
                                         
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 col-xs-100pc p-tb20 ">
                                    <div class="wt-icon-box-wraper left">
                                        <div class="icon-sm text-primary">
                                            <a href="#" class="icon-cell p-t5 center-block"><i class="fa fa-picture-o" aria-hidden="true"></i></a>
                                        </div>
                                        <div class="icon-content">
                                            <h5 class="wt-tilte text-uppercase m-b0">interior</h5>
                                         
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
            <!-- ABOUT COMPANY SECTION END -->                      
</section>


<section id="services">
	    <!-- COMPANY DETAIL SECTION START -->
            <div class="section-full p-t50 p-b50 overlay-wraper bg-parallax"  style="background-image:url(images/background/bg-5.jpg);">
                <div class="overlay-main opacity-07 bg-black"></div>
                <div class="container ">
                    <div class="row">
                        <div class="col-md-4"></div>
                        <div class="col-md-8">
                        	<div class="text-right text-white">
                            	<h3 class="font-24">The Construction Company</h3>
                                <h1 class="font-60">OUR SERVICES</h1>
                                <p align="justify">At Rupert Building Services, science drives us while integrity guides us. With our deep bench of technical consultants in engineering and architectural disciplines, we provide a comprehensive range of services designed to optimize the performance, maintenance and repair of buildings, plants, facilities, and infrastructure. We don't strive to just meet minimum code requirements, but rather, to maximize the life cycle of your facilities.</p>
                            </div>
                         </div>
                        <div class="col-md-6"></div>
                        <div class="col-md-2 col-sm-4">
                            <div class="status-marks  text-white m-tb10">
                                <div class="status-value text-right">
                                	<span class="counter">1150</span>
                                    <i class="fa fa-building font-26 m-l15"></i>
                                </div>
                                <h6 class="text-uppercase text-white text-right">PROJECT COMPLETED</h6>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <div class="status-marks  text-white m-tb10">
                                <div class="status-value text-right">
                                	<span class="counter">2223</span>
                                    <i class="fa fa-users font-26 m-l15"></i>
                                </div>
                                <h6 class="text-uppercase text-white text-right">HAPPY CLIENTS</h6>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-">
                            <div class="status-marks  text-white m-tb10">
                                <div class="status-value text-right">
                                	<span class="counter">4522</span>
                                    <i class="fa fa-user-plus font-26 m-l15"></i>
                                </div>
                                <h6 class="text-uppercase text-white text-right">WORKERS EMPLOYED</h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- COMPANY DETAIL SECTION End -->  

</section>


<section id="contact">
	

	  <!-- SECTION CONTENTG START -->
            <div class="section-full p-t80 p-b50">
                <div class="container">
                
                	<!-- CONTACT DETAIL BLOCK -->
                    <div class="section-content m-b30">
 
                        <div class="row">
                        
                            <div class="col-md-4 col-sm-12 m-b30">
                                <div class="wt-icon-box-wraper center p-a30 bg-secondry">
                                    <div class="icon-sm text-white m-b10"><i class="iconmoon-smartphone-1"></i></div>
                                    <div class="icon-content">
                                        <h5 class="text-white">Phone number</h5>
                                        <p class="text-gray-dark"></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 m-b30">
                                <div class="wt-icon-box-wraper center p-a30 bg-secondry">
                                    <div class="icon-sm text-white m-b10"><i class="iconmoon-email"></i></div>
                                    <div class="icon-content">
                                        <h5 class="text-white">Email address</h5>
                                        <p class="text-gray-dark">info@rupertservices.co.uk</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12 m-b30">
                                <div class="wt-icon-box-wraper center p-a30 bg-secondry">
                                    <div class="icon-sm text-white m-b10"><i class="iconmoon-travel"></i></div>
                                    <div class="icon-content">
                                        <h5 class="text-white">Address info</h5>
                                        <p class="text-gray-dark">20 Rupert Gardens, London, Sw97tl.</p>
                                    </div>
                                </div>
                            </div>
                        
                        </div>
          
                    </div>
                    
                    <!-- GOOGLE MAP & CONTACT FORM -->
                    <div class="section-content m-b50">
                        <div class="row">
                        
                        	<!-- LOCATION BLOCK-->
                            <div class="wt-box col-md-12">
                            
                                 <h4 class="text-uppercase">Location</h4>
                                <div class="wt-separator-outer m-b30">
                                   <div class="wt-separator style-square">
                                       <span class="separator-left bg-primary"></span>
                                       <span class="separator-right bg-primary"></span>
                                   </div>
                               </div>      
                                                        
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2485.4446627255315!2d-0.10668058469324676!3d51.468351821379116!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4876047cbb49869b%3A0xe794f265603e1345!2s20%20Rupert%20Gardens%2C%20Brixton%2C%20London%20SW9%207TL%2C%20UK!5e0!3m2!1sen!2sin!4v1589371870946!5m2!1sen!2sin" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                              
                                    
                            </div>

                    </div>
                    
                </div>
           </div>
            <!-- SECTION CONTENT END -->
        </div>
		</section>



@endsection